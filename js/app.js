let expenses = [];

let validateSet = document.querySelector('#formSubmit')
validateSet.addEventListener('submit', function (event) {

    event.preventDefault()

    let getLabel = document.querySelector('#label')
    let getPrice = document.querySelector('#price')
    let getDate = document.querySelector('#date')

    setExpense(getLabel.value, Number(getPrice.value), getDate.value)

    event.target.reset()
    $('#modalForm').modal('hide') //https://getbootstrap.com/docs/4.5/components/modal/#modalhide//

})

function setExpense(setLabel, setPrice, setDate) {

    let Expense = {
        label: null,
        price: 0,
        date: null,
    }

    let setExpense = Object.create(Expense)
    setExpense.label = setLabel
    setExpense.price = setPrice
    setExpense.date = setDate

    expenses.push(setExpense)

    refreshUI();
}

function removeExpense(table, element) {

    table.splice(element, 1)

    refreshUI();
}

function verifLabel(label) {

    let test = label.trim()

    if (test === "") {
        return "Saisi incorrect."
    }
}

function verifPrice(price) {

    let test = Number(price.trim())

    if (isNaN(test) || test === "") {
        return "Ce n'est pas un prix."
    }

}

function updateExpense(table, element) {}

function createTable(table) {

    for (let element = 0; element < table.length; element++) {

        let listExpense = document.querySelector('.list-group')

        let createLI = document.createElement('li')
        createLI.className = "list-group-item list-group-item-action d-flex justify-content-between align-items-center"

        let createDivPrice = document.createElement('div')
        createDivPrice.className = "d-flex align-items-center"

        let createDivLabel = document.createElement('div')
        createDivLabel.className = "d-flex align-items-center"

        let createDivLabelDate = document.createElement('div')
        createDivLabelDate.className = "div-label"

        let createLabelSpan = document.createElement('span')
        createLabelSpan.className = "label"
        createLabelSpan.textContent = table[element].label

        let createNumberSpan = document.createElement('span')
        createNumberSpan.className = "mr-5"
        createNumberSpan.textContent = [element + 1] + ". "

        let createP = document.createElement('p')
        createP.textContent = table[element].date

        let createPriceSpan = document.createElement('span')
        createPriceSpan.className = "badge badge-pill mr-5 price p-2"
        createPriceSpan.textContent = table[element].price + "€"


        let createButtonDelete = document.createElement('button')
        createButtonDelete.setAttribute('type', 'button')
        createButtonDelete.className = "btn btn-danger-color"
        createButtonDelete.textContent = "Supprimer"

        createButtonDelete.addEventListener("click", function () {
            createLI.remove()
            removeExpense(table, element)
        })

        listExpense.appendChild(createLI).append(createDivLabelDate, createDivPrice)
        createDivLabelDate.append(createDivLabel, createP)
        createDivLabel.append(createNumberSpan, createLabelSpan)
        createDivPrice.append(createPriceSpan, createButtonDelete)

    }

}

function createTotals(table) {

    let totalPrice = 0;
    for (const element of table) {
        totalPrice += element.price
    }

    let container = document.querySelector('#mainList')

    let createTotalPrice = document.createElement('div')
    createTotalPrice.className = "total-result badge shadow-lg d-flex justify-content-around"
    container.appendChild(createTotalPrice)

    createTotalPrice.textContent = "Total des dépenses : " + totalPrice + "€"

}

function numberExpenses(table) {

    let numberExpenses = document.querySelector('#infoExpense')
    if (table.length <= 0) {
        numberExpenses.textContent = "Vous n'avez aucunes dépenses."
    } else if (table.length === 1) {
        numberExpenses.textContent = "Vous avez " + table.length + " dépense."
    } else {
        numberExpenses.textContent = "Vous avez " + table.length + " dépenses."
    }

}

function refreshUI() {

    numberExpenses(expenses)

    let refreshLI = document.querySelectorAll('li')
    for (let i = 0; i < refreshLI.length; i++) {
        refreshLI[i].remove()
    }

    let refreshTotal = document.querySelector('.total-result')
    refreshTotal.remove()

    createTable(expenses);
    createTotals(expenses);

}

numberExpenses(expenses);
createTable(expenses);
createTotals(expenses);













