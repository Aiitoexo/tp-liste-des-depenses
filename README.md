# Application de suivi des dépenses




## Cahier des charges

En tant qu'utilisateur de l'app, je veux pouvoir :

    - Ajouter un participant
    - Supprimer un participant
    - Voir la liste des participants
    - Editer un participant
    - Ajouter une dépense
    - Modifier une dépense
    - Supprimer une dépense
    - Attribuer la dépense à des participants
    - voir la répartition des remboursements à tout instant 
    - avoir une liste de dépenses "standards" 
    - ? Enregistrer un remboursement à une dépense 
    - ? Simplifier les opérations de remboursement (redirections des paiements)
    
## Entités

### Participant

    - Name (pseudo..)
    - ? Photo 
    
### Dépense
    
    - montant en euros  
    - designation 
    - date d'achat
    - acheteur (celui qui a payé la dépense)
    - liste des personnes qui doivent rembourser
    - ? liste des remboursement effectués
    
    
## Etapes du développement :

1. **CRUD** des dépenses
    - *R*ead: afficher la liste des dépense
    - *C*reate  
    - *D*elete  
    - *U*pdate  
    
    